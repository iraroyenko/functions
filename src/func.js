const getSum = (str1, str2) => {
  let st1 = Number.parseInt(str1);
  let st2 = Number.parseInt(str2);
  if(Number.isNaN(st1))
  {
    if(Number.isNaN(st2)) return false;
    else return str2;
  }
  if(Number.isNaN(st2)) return str1;
  return (st1+st2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for(let post of listOfPosts)
  {
    if(post.author == authorName)
    {
      posts++;
    }
    if(post.comments)
    {
      post.comments.forEach(a => {
        if(a.author == authorName)
          comments++;
      });
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let sum = 0;
  for(let p of people)
  {
    if(Number(p) - 25 > sum) return 'NO';
    sum += 25;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
